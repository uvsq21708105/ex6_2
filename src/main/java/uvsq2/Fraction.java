package uvsq2;

import Fraction;

public class Fraction {

	public int numerateur ;
	public int denuminateur ;
	final int zero =0;
	final int un=1;
	
	public Fraction(int numerateur, int denuminateur) {
		super();
		this.numerateur = numerateur;
		this.denuminateur = denuminateur;
	}

	public Fraction() {
	this.numerateur=zero;
		this.denuminateur=un;
	}

	public Fraction(int numerateur) {
		super();
		this.numerateur = numerateur;
		this.denuminateur=un;
	}

	public int getNumerateur() {
		return numerateur;
	}

	
	public int getDenuminateur() {
		return denuminateur;
	}

	public double vergule()
	{
		
		
		return (double) this.numerateur/this.denuminateur ;
	}
	
	public Fraction addition(Fraction f1, Fraction f2)
	{
		Fraction f = new Fraction();
		f.numerateur = (f1.numerateur*f2.denuminateur) + (f2.numerateur*f1.denuminateur);
		f.denuminateur = f1.denuminateur*f2.denuminateur;
		return f;
	}
	
	public boolean egalite(Fraction f1, Fraction f2)
	{
		if (f1.numerateur*f2.denuminateur == f1.denuminateur*f2.numerateur)
			return true ;
		return false;
	}
	@Override
	public String toString() {
		return  numerateur + "/"	+ denuminateur ;
	}
	
	public String naturel(Fraction f1, Fraction f2){
		if (egalite(f1,f2)==true)
		return "les fractions sont egaux";
		if (f1.vergule() < f2.vergule())
			return "la premiere fraction est plus petite";
		else
		return "la deuxieme fraction est plus petite ";
	}
	
	
	
}
